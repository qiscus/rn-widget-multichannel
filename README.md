# Implement multichannel widget in RN Project(Expo)

## Demo App
![Alt text](http://i.imgur.com/oSNbAMw.png "Scan With Expo")

https://expo.io/@mryoga/qiscus-multichannel-widget
## Installation
- `npm install -g expo-cli`
- `git clone https://yogasetiawan@bitbucket.org/qiscus/rn-widget-multichannel.git`
- `cd rn-widget-multichannel`
- `npm install`

## Running with Expo
First run `npm start`

Then scan the QR code with your installed Expo app.

