import React from "react";
import {Text, TouchableOpacity, View} from "react-native";

export default class ChatAgentScreen extends React.Component {
    render() {
        let {params} = this.props.navigation.state
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("WebViewScreen", params)}>
                    <View style={{
                        backgroundColor: "#56d55f",
                        padding: 20,
                        borderRadius: 10
                    }}>
                        <Text style={{color: "white", fontSize: 20, fontFamily: "sans-serif"}}>Talk To Agent</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
