import React from "react";
import {ActivityIndicator} from "react-native";
import {WebView} from "react-native-webview";

export default class WebViewScreen extends React.Component {
    _onNavigationChange = ({url}) =>{
        let {params} = this.props.navigation.state
        if (url.includes("#logout")){
            this.props.navigation.navigate("ChatAgentScreen", params)
        }
    }
    indicatorloading = () => {
        return (
            <ActivityIndicator
                color={"#00ee87"}
                size={"large"}
                style={{
                    position: "absolute",
                    right: 0,
                    left: 0,
                    bottom: 0,
                    top: 0
                }}
            />
        )
    }
    render() {
        let {params} = this.props.navigation.state
        let url =   `https://qiscus-custom-multichannel.web.app/index.html?email=${params.email}&username=${params.username}`
        return <WebView
            style={{marginTop:24}}
            renderLoading={this.indicatorloading}
            startInLoadingState={true}
            onNavigationStateChange={this._onNavigationChange.bind(this)}
            source={{uri: url}}/>
    }
}
