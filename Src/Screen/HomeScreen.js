import React from "react";
import {Button, Text, TextInput, View} from "react-native";

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: "guest",
            email: "email@mail.com"
        }
    }

    render() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{width:"50%"}}>
                    <Text style={{alignSelf:"center", marginBottom:50, fontSize:20, fontWeight:"bold"}}>Form Data</Text>
                    <Text>Input Email : </Text>
                    <TextInput
                        style={{
                            width: "100%",
                            marginBottom:10
                        }}
                        value={this.state.email}
                        underlineColorAndroid={"#00ee87"}
                        onChangeText={email => this.setState({email})}/>
                    <Text>Input Username : </Text>
                    <TextInput
                        style={{
                            width: "100%"
                        }}
                        value={this.state.username}
                        underlineColorAndroid={"#00ee87"}
                        onChangeText={username => this.setState({username})}/>

                    <Button
                        title={"Login"}
                        color={"#00ee87"}
                        onPress={() =>
                            this.props.navigation.navigate("ChatAgentScreen",{
                                username:this.state.username,
                                email:this.state.email
                            })}
                    />
                </View>
            </View>
        );
    }
}
