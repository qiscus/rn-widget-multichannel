import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from "../Screen/HomeScreen";
import WebViewScreen from "../Screen/WebViewScreen";
import ChatAgentScreen from "../Screen/ChatAgentScreen";

const AppNavigator = createStackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            title: "Home"
        }
    },
    ChatAgentScreen: {
        screen: ChatAgentScreen,
        navigationOptions: {
            title: "Chat Agent"
        }
    },
    WebViewScreen: {
        screen: WebViewScreen,
        navigationOptions: {
            header:null
        }
    }
});

export default createAppContainer(AppNavigator);
